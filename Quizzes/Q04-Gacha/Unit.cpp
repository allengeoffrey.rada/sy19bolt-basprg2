#include "Unit.h"

void Unit::takeDamage(int value)
{
	// Decrease health and prevent it from becoming lower than 0
	mHP -= value;
	if (mHP < 0) mHP = 0;
	
	cout << "You take " << value << " damage." << endl;
}

void Unit::heal(int value)
{
	mHP += value;

	cout << "You are healed by " << value << " HP." << endl;
}

void Unit::useCrystals(int value)
{
	mCrystals -= value;
}

void Unit::addCrystals(int value)
{
	mCrystals += value;

	cout << "You get " << value << " crystals." << endl;
}

int Unit::getRarityPoints()
{
	return mRarityPoints;
}

void Unit::addRarityPoints(int value)
{
	mRarityPoints += value;

	cout << "You get " << value << " rarity points." << endl;
}

void Unit::addPull()
{
	mPulls++;
}

const vector<string>& Unit::getPulledItems()
{
	return mPulledItems;
}

void Unit::addPulledItem(string value)
{
	mPulledItems.push_back(value);
}

void Unit::printStats()
{
	cout << "HP: " << mHP << endl;
	cout << "Crystals: " << mCrystals << endl;
	cout << "Rarity Points: " << mRarityPoints << endl;
	cout << "Pulls: " << mPulls << endl;
}

bool Unit::isAlive()
{
	if (mHP == 0) return false;
	else return true;
}

bool Unit::isBroke()
{
	// Check if player can pay required crystals per pull (5 crystals)
	if (mCrystals < 5) return true;
	else return false;
}
