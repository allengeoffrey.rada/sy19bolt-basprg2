#include "CurrencyItem.h"

CurrencyItem::CurrencyItem(string name, int weight, int value)
	: Item(name, weight)
{
	mValue = value;
}

void CurrencyItem::activate(Unit* target)
{
	target->addCrystals(mValue);
}
