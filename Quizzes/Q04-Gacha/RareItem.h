#pragma once
#include "Item.h"
class RareItem :
    public Item
{

public:
    RareItem(string name, int weight, int value);

    void activate(Unit* target);

private:
    int mValue;

};
