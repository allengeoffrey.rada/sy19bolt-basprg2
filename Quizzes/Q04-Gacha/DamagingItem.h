#pragma once
#include "Item.h"
class DamagingItem :
    public Item
{

public:
    DamagingItem(string name, int weight, int value);

    void activate(Unit* target);

private:
    int mValue;

};

