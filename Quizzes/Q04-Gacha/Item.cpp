#include "Item.h"

Item::Item(string name, int weight)
{
	mName = name;
	mWeight = weight;
}

string Item::getName()
{
	return mName;
}

int Item::getWeight()
{
	return mWeight;
}
