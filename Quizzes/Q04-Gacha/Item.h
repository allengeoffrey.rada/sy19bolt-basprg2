#pragma once
#include <iostream>
#include "Unit.h"

using namespace std;

class Item
{

public:
	Item(string name, int weight); // Item constructor

	// Getters
	string getName();
	int getWeight();

	virtual void activate(Unit* actor) = 0; // Virtual function for derived classes to use

private:
	string mName;
	int mWeight; // Weight in loot table

};

