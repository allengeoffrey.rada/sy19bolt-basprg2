#include "RareItem.h"

RareItem::RareItem(string name, int weight, int value)
	:Item(name, weight)
{
	mValue = value;
}

void RareItem::activate(Unit* target)
{
	target->addRarityPoints(mValue);
}
