#pragma once
#include "Item.h"
class HealingItem :
    public Item
{

public:
    HealingItem(string name, int weight, int value);

    void activate(Unit* target);

private:
    int mValue;

};

