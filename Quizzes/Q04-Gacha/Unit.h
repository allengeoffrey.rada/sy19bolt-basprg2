#pragma once
#include <iostream>
#include <vector>

using namespace std;

class Unit
{

public:

	// Getters and intuitive functions
	// For health
	void takeDamage(int value);
	void heal(int value);

	// For crystals
	void useCrystals(int value);
	void addCrystals(int value);

	// For rarity points
	int getRarityPoints();
	void addRarityPoints(int value);

	// For pulls and pulled items
	void addPull();
	const vector<string>& getPulledItems();
	void addPulledItem(string value);

	// Other methods
	void printStats();
	bool isAlive(); // Check if unit is alive
	bool isBroke(); // Check if unit has enough crystals to play

private:
	// Starting values
	int mHP = 100;
	int mCrystals = 100;
	int mRarityPoints = 0;

	int mPulls = 0; // Keep track of how many pulls
	vector<string> mPulledItems; // Keep track of items pulled (only names because we don't need their item functionality at this point)

};

