#include <iostream>
#include <time.h>
#include <vector>

#include "Item.h"
#include "CurrencyItem.h"
#include "DamagingItem.h"
#include "HealingItem.h"
#include "RareItem.h"
#include "Unit.h"

using namespace std;

vector<Item*> createLootTable();
Item* pullFromLootTable(const vector<Item*>& lootTable);
int countItem(const vector<string>& items, string item);
bool playRound(Unit* player, const vector<Item*> lootTable);

int main()
{
	
	srand(time(0));

	Unit* player = new Unit(); // Create new unit 

	vector<Item*> lootTable = createLootTable(); // Create loot table

	bool result = true; // True if player can still play

	while (result) // Play as long as player hasn't met any winning / losing conditions
	{

		result = playRound(player, lootTable); // Play round with player and created loot table

	}

	// Check if player won (have 100+ rarity points)
	if (player->getRarityPoints() >= 100) cout << "You win!" << endl;
	else cout << "You lose!" << endl;

	// Print player final stats
	player->printStats();

	vector<string> pulledItems = player->getPulledItems(); // Get pulled items

	// Print items pulled and how many for each item
	cout << "Items pulled:" << endl;
	for (int i = 0; i < lootTable.size(); i++)
	{
		cout << countItem(pulledItems, lootTable[i]->getName()) << "x " << lootTable[i]->getName() << endl;
	}

	// Delete loot table contents
	for (int i = 0; i < lootTable.size(); i++)
	{
		delete lootTable[i];
		lootTable[i] = nullptr;
	}

	// Delete player unit
	delete player;
	player = nullptr;

}

vector<Item*> createLootTable()
{
	vector<Item*> table; // Vector for storing the items

	// Create items, weight is second parameter
	RareItem* rareItemR = new RareItem("R", 40, 1);
	RareItem* rareItemSR = new RareItem("SR", 9, 10);
	RareItem* rareItemSSR = new RareItem("SSR", 1, 50);
	HealingItem* healthPotion = new HealingItem("Health Potion", 15, 30);
	DamagingItem* bomb = new DamagingItem("Bomb", 20, 25);
	CurrencyItem* crystal = new CurrencyItem("Crystal", 15, 15);

	// Add items to table
	table.push_back(rareItemR);
	table.push_back(rareItemSR);
	table.push_back(rareItemSSR);
	table.push_back(healthPotion);
	table.push_back(bomb);
	table.push_back(crystal);

	return table; // Return loot table to caller
}

Item* pullFromLootTable(const vector<Item*>& lootTable)
{

	int totalWeight = 0; // In order to get max number to generate
	
	// Add all weights of the items in the loot table
	for (int i = 0; i < lootTable.size(); i++)
	{
		totalWeight += lootTable[i]->getWeight();
	}

	int randomNumber = rand() % totalWeight + 1; // Generate a number from 1 to totalWeight (in this case, 1 to 100)

	int cumulativeWeight = 0; 
	// Added to weight to go over already checked items 
	// First item would have a range of 1 to itemWeight (1 - 40 for the item "R")
	// Next items would have a range of cumulativeWeight to itemWeight + cumulativeWeight (41 - 49 for the item "SR", etc.)

	for (int i = 0; i < lootTable.size(); i++)
	{
		// Check if random number is in range (random number < 41, random number < 50, etc.)
		if (randomNumber <= lootTable[i]->getWeight() + cumulativeWeight) 
		{
			return lootTable[i]; // Return respective item
		}

		// Add current weight to check the next range (add 40 to 9 to check 41 to 49, etc.)
		cumulativeWeight += lootTable[i]->getWeight();
	}

}

int countItem(const vector<string>& items, string item)
{
	int count = 0; // Counter
	for (int i = 0; i < items.size(); i++) // Loop through the whole vector
	{
		if (items[i] == item) // If name matches parameter, increment count
		{
			count++;
		}
	}

	return count; // Return count
}

bool playRound(Unit* player, const vector<Item*> lootTable)
{

	player->printStats(); // Display player stats

	Item* pull = pullFromLootTable(lootTable); // Pull an item from the loot table

	cout << "You pulled a " << pull->getName() << "." << endl;

	pull->activate(player); // Activate pulled item effect

	// Add pulls and add pulled item to player's pulled items
	player->addPull();
	player->addPulledItem(pull->getName());

	// Deduct crystals
	player->useCrystals(5);

	system("pause");
	system("cls");

	// Check lose conditions (is player dead or broke) and win condition (has 100+ rarity points)
	// Returns false to stop playing or true to continue playing
	if (!player->isAlive() || player->isBroke() || player->getRarityPoints() >= 100) return false;
	else return true;

}
