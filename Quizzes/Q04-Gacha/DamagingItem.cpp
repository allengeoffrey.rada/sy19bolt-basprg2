#include "DamagingItem.h"

DamagingItem::DamagingItem(string name, int weight, int value)
	: Item(name, weight)
{
	mValue = value;
}

void DamagingItem::activate(Unit* target)
{
	target->takeDamage(mValue);
}
