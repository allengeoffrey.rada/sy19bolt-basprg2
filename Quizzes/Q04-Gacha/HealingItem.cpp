#include "HealingItem.h"

HealingItem::HealingItem(string name, int weight, int value)
	: Item(name, weight)
{
	mValue = value;
}

void HealingItem::activate(Unit* target)
{
	target->heal(mValue);
}
