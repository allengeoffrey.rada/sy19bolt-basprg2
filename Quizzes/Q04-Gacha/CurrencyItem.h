#pragma once
#include "Item.h"
class CurrencyItem :
    public Item
{

public:
    CurrencyItem(string name, int weight, int value);

    void activate(Unit* target);

private:
    int mValue;

};
