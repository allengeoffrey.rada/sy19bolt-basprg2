#include <iostream>
#include <vector>
#include <time.h>

using namespace std;

void createDeck(vector<string>& deck, string kind);
void playRound(int round, int& moneyEarned, int& mmLeft);
int askForBet(int mmLeft);
string playerPlayCard(const vector<string>& deck);
string enemyPlayCard(const vector<string>& deck);
string compareCards(string playerCard, string enemyCard);
void removeCard(string card, vector<string>& deck);
void evaluateRound(string result, string playerSide, int& moneyEarned, int& mmLeft, int bet);
void evaluateEnding(int moneyEarned, int mmLeft);

int main()
{
	srand(time(0));

	// Initialize variables
	int round = 1; // Starts at round 1
	int mmLeft = 30;
	int moneyEarned = 0;

	// Continue playing until last round or if victoryConditions are met
	while (mmLeft > 0 || round > 12)
	{
		// Play the round
		playRound(round, moneyEarned, mmLeft);

		// Round ended. Increment round
		round++;
	}

	// Determine player ending
	evaluateEnding(moneyEarned, mmLeft);

	cout << endl;
	return 0;
}

void createDeck(vector<string>& deck, string kind)
{
	deck.push_back(kind); // Add the emperor/slave card
	// Add the 4 civilians
	for (int i = 0; i < 4; i++)
	{
		deck.push_back("Civilian");
	}
}

void playRound(int round, int& moneyEarned, int& mmLeft)
{

	// Declaring variables
	vector<string> playerDeck;
	vector<string> enemyDeck;

	string playerSide;
	string enemySide;

	string playerCard;
	string enemyCard;

	// Default Draw value to enter while loop
	string result = "Draw";

	int playerBet;

	// Check round and determine which side player is on
	// Can be done more elegantly(?)
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		playerSide = "Emperor";
		enemySide = "Slave";
	}
	else
	{
		playerSide = "Slave";
		enemySide = "Emperor";
	}

	// Create decks for the player and the enemy
	createDeck(playerDeck, playerSide);
	createDeck(enemyDeck, enemySide);

	// Print player details
	cout << "There's " << mmLeft << "mm between your eardrum and the drill." << endl;
	cout << "You currently have " << moneyEarned << " yen." << endl;
	cout << "It is currently round " << round << "." << endl;
	cout << "You are on the " << playerSide << " side." << endl;

	// Ask player for bet
	playerBet = askForBet(mmLeft);

	system("cls");

	// Main loop, ends if one side wins (plays emperor or slave)
	while (result == "Draw")
	{
		// Show player bet
		cout << "You are betting " << playerBet << "mm." << endl;

		// Play cards 
		string playerCard = playerPlayCard(playerDeck);
		string enemyCard = enemyPlayCard(enemyDeck);

		cout << "You picked: " << playerCard << endl;
		cout << "The enemy picked: " << enemyCard << endl;

		// Remove cards from deck
		removeCard(playerCard, playerDeck);
		removeCard(enemyCard, enemyDeck);

		// Compare cards
		result = compareCards(playerCard, enemyCard);

		system("pause");
		system("cls");

	}

	// Evaluate round and add money earned and deduct millimeters if necessary
	evaluateRound(result, playerSide, moneyEarned, mmLeft, playerBet);

}

int askForBet(int mmLeft)
{
	int bet;

	cout << "How many milimeters are you betting?" << endl;
	while (true)
	{
		cin >> bet;
		// Check for invalid input
		if (bet > mmLeft || bet <= 0)
		{
			cout << "You have entered an invalid input." << endl;
		}
		else
		{
			break;
		}
	}
	
	return bet;
}

string playerPlayCard(const vector<string>& deck)
{
	// Print cards in deck
	cout << "Here are your cards." << endl;
	for (int i = 0; i < deck.size(); i++)
	{
		cout << "[" << i << "]: " << deck[i] << endl;
	}

	// Play a card
	int playerChoice;
	cout << "Play a card by inputting the number corresponding to the card." << endl;
	while (true)
	{
		cin >> playerChoice;
		// Check for invalid input
		if (playerChoice < 0 || playerChoice >= deck.size())
		{
			cout << "You have entered an invalid input." << endl;
		}
		else
		{
			break;
		}
	}

	return deck[playerChoice];
}

string enemyPlayCard(const vector<string>& deck)
{
	// Just return a random card from the remaining deck
	return deck[rand() % +deck.size()];
}

string compareCards(string playerCard, string enemyCard)
{
	// Determine winner
	/*
	Emperor beats Civilian
	Civilian beats Slave
	Slave beats Emperor
	Civilian against civilian results in a tie
	*/
	if (playerCard == enemyCard)
	{
		cout << "Both players played a civilian, nothing happens." << endl;
		return "Draw";
	}
	else if (playerCard == "Emperor" && enemyCard == "Slave")
	{
		cout << "You played the emperor against the slave! You lose this round." << endl;
		return "Lose";
	}
	else if (playerCard == "Slave" && enemyCard == "Emperor")
	{
		cout << "You played the slave against the emperor! You win this round." << endl;
		return "Win";
	}
	else if (playerCard == "Emperor")
	{
		cout << "You played the emperor against a civilian! You win this round." << endl;
		return "Win";
	}
	else if (playerCard == "Slave")
	{
		cout << "You played the slave against a civilian! You lose this round." << endl;
		return "Lose";
	}
	else if (playerCard == "Civilian" && enemyCard == "Emperor")
	{
		cout << "You played a civilian against the emperor! You lose this round." << endl;
		return "Lose";
	}
	else
	{
		cout << "You played a civilian against the slave! You win this round." << endl;
		return "Win";
	}
}

void removeCard(string card, vector<string>& deck)
{
	// Remove a single copy of the played card
	for (int i = 0; i < deck.size(); i++)
	{
		if (deck[i] == card)
		{
			deck.erase(deck.begin() + i);
			return;
		}
	}
		
}

void evaluateRound(string result, string playerSide, int& moneyEarned, int& mmLeft, int bet)
{
	// Evaluate the round
	if (result == "Win")
	{
		if (playerSide == "Emperor")
		{
			// Normal payout
			moneyEarned += bet * 100000;
		}
		else 
		{
			// Five times payout due to slave bonus
			moneyEarned += bet * 5 * 100000;
		}
	}
	else
	{
		// Reduce millimeters left based on bet if player loses
		mmLeft -= bet;
	}
}

void evaluateEnding(int moneyEarned, int mmLeft)
{
	if (mmLeft > 0)
	{
		if (moneyEarned >= 20000000)
		{
			cout << "Congratulations, you earned " << moneyEarned << " yen and managed to keep your eardrum intact." << endl;
			cout << "Result: Best Ending." << endl;
		}
		else
		{
			cout << "Congratulations, you managed to keep your eardrum intact... but you only won " << moneyEarned << " yen." << endl;
			cout << "Result: Meh Ending." << endl;
		}
	}
	else
	{
		cout << "You won " << moneyEarned << " yen at the cost of one of your ears." << endl;
		cout << "Result: Bad Ending." << endl;
	}
}
