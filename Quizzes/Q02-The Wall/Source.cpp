#include <iostream>
#include <time.h>
#include "Node.h"

using namespace std;

Node* createWatchers(int size);
Node* removeWatcher(Node* watchers);
int getNumberOfWatchers(Node* watchers);
void printWatchers(Node* watchers);
Node* passCloakRandomly(Node* watchers);
Node* gameLoop(Node* watchers);

int main()
{
	srand(time(0));

	// Round counter for display purposes
	int round = 1;

	// Ask player for number of soldiers
	int numberOfWatchers;
	cout << "How many watchers on watch today?" << endl;
	while (true)
	{
		cin >> numberOfWatchers;
		// Check for invalid input
		if (numberOfWatchers < 1)
		{
			cout << "You have entered an invalid input." << endl;
		}
		else
		{
			break;
		}
	}

	// Create a circular linked list based on user input
	Node* watchers = createWatchers(numberOfWatchers);

	system("cls");

	cout << "The watchers have lined up at the wall:" << endl;

	printWatchers(watchers);
	
	cout << "Let's pass around the cloak." << endl;

	// Pass the cloak to a random watcher
	watchers = passCloakRandomly(watchers);

	cout << watchers->name << " is now holding the cloak." << endl;

	cout << "Let's begin the game." << endl;

	system("pause");
	system("cls");

	while (getNumberOfWatchers(watchers) > 1) // Keep playing until one watcher remains
	{
		cout << "Round "<< round << endl;
		watchers = gameLoop(watchers);
		round++;

		system("pause");
		system("cls");
	}

	cout << "Only " << watchers->name << " remains. " << watchers->name << " will now seek reinforcements." << endl;

	delete watchers;
	watchers = nullptr;

}

Node* createWatchers(int size)
{
	// Declare variables for a linked list
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;
	string name;
	
	// Loop for creating any size circular linked list
	for (int i = 0; i < size; i++)
	{
		// Ask for name
		cout << "Input name for your soldier:" << endl;
		cin >> name;

		// Create the node
		current = new Node;
		current->name = name; // Set node name
		if(i == 0)head = current; // Set node as the head if it is the first node
		if (i > 0)
		{
			current->previous = previous;
			previous->next = current; // Set previous node's next node to the current node if applicable
		}
			
		previous = current; // Set the current node as previous node for the next node
		if (i == size - 1)current->next = head; // Set next node to the head node if current node is the last node
	}
	
	head->previous = previous; // Set head's previous node to the last node

	return head;
}

Node* removeWatcher(Node* watchers)
{
	Node* toRemove = watchers; // Create node that would point to node to be removed
	toRemove->next->previous = toRemove->previous; // Set the previous node of the next node to be the current previous node
	toRemove->previous->next = toRemove->next; // Set the next node of the previous node to be the current next node
	watchers = toRemove->next; // Set next person to be the cloak holder

	// Print out eliminated watcher before deleting the node
	cout << toRemove->name << " was eliminated and the cloak is passed to " << toRemove->next->name << "." << endl; 

	// Remove pointer and point to null
	delete toRemove;
	toRemove = nullptr;

	return watchers;

}

int getNumberOfWatchers(Node* watchers)
{
	Node* head = watchers; // Remember head node to check later
	int count = 0; // Variable for storing the number of members

	// Loop through the list
	do
	{
		watchers = watchers->next; // Start with the next node to not end the loop immediately
		count++; // Increment number of watchers
	} while (head != watchers); // If the loop goes through the head it means it got through all the members

	return count;
}

void printWatchers(Node* watchers)
{
	int count = 0; // Counts how many passes has been made

	cout << "List of watchers remaining:" << endl;
	cout << "Cloak holder: " << watchers->name << endl; // Prints the one holding the cloak

	// Loop through the list until the count matches the list size
	while (count != getNumberOfWatchers(watchers))
	{
		cout << watchers->name << endl;
		watchers = watchers->next; // Move to next node
		count++; // Increment count/passes
	}
}

Node* passCloakRandomly(Node* watchers)
{
	int randomNumber = rand() % getNumberOfWatchers(watchers) + 1; // Pass the cloak randomly based on list size
	int count = 0; // Counts how many passes has been made

	// Pass the cloak until the count matches the random number
	while (randomNumber != count)
	{
		watchers = watchers->next; // Move to next node
		count++; // Increment count/passes
	}

	cout << "The cloak is passed " << randomNumber << " times and is now on " << watchers->name << "." << endl;

	return watchers;
}

Node* gameLoop(Node* watchers)
{

	printWatchers(watchers); // Display remaining watchers
	watchers = passCloakRandomly(watchers); // Pass the cloak randomly
	watchers = removeWatcher(watchers); // then remove the watcher holding the cloak, then pass the cloak to the next person
	cout << "Number of watchers left: " << getNumberOfWatchers(watchers) << endl; // Number of watchers left after removing a watcher

	return watchers;

}
