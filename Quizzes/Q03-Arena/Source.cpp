#include <iostream>
#include <time.h>
#include "Unit.h"

using namespace std;

Unit* createPlayer();
Unit* createEnemy(int round);
void printStats(Unit* unit);
void playRound(Unit* player, Unit* enemy);
void playTurn(Unit* actor, Unit* target);

int main()
{
	srand(time(0));

	int round = 0; // Keep track of round

	Unit* player = createPlayer(); // Create player unit
	Unit* enemy = nullptr; // Pointer to enemy unit

	system("cls"); // Clear character creation

	while (!player->isDead()) // Keep playing while player is not dead
	{
		round++; // Increment round number
		enemy = createEnemy(round); // Create enemy unit with scaling stats based on round number
		cout << "ROUND " << round << endl;
		playRound(player, enemy); // Play round with two battling units

		// Delete enemy 
		delete enemy; 
		enemy = nullptr;
	}

	// Print ending
	cout << player->getName() << " has died on the arena." << endl;
	cout << player->getName() << " lasted " << round << " rounds." << endl;
	printStats(player);

	// Delete player
	delete player;
	player = nullptr;

}

Unit* createPlayer()
{

	// Variables for unit creation
	string name;
	int choice;
	string unitClass;

	// Ask for player name
	cout << "What is your name?" << endl;
	cin >> name;

	// Ask for player class
	cout << "Select your class" << endl;
	cout << "[1] Warrior" << endl << "[2] Assassin" << endl << "[3] Mage" << endl;
	while (true) // Check for invalid input
	{
		cin >> choice;
		if (choice > 3 || choice < 1)
		{
			cout << "Please enter a valid input (1-3)" << endl;
		}
		else
		{
			break;
		}
	}

	switch (choice) // Determine unit class based on choice
	{
		case 1:
			unitClass = "Warrior";
			break;
		case 2:
			unitClass = "Assassin";
			break;
		case 3:
			unitClass = "Mage";
			break;
	}

	Unit* unit = new Unit(name, unitClass); // Create unit
	return unit; // Return created unit to caller
}

Unit* createEnemy(int round)
{
	// Variables for unit creation
	string name;
	int choice = rand() % 3 + 1; // Randomize unit class selection
	string unitClass;

	switch (choice) // Determine class based on choice
	{
	case 1:
		unitClass = "Warrior";
		break;
	case 2:
		unitClass = "Assassin";
		break;
	case 3:
		unitClass = "Mage";
		break;
	}

	name = "Enemy " + unitClass; // Create enemy named based on class (Enemy + unit class)

	Unit* unit = new Unit(name, unitClass, round); // Create unit
	return unit; // Return created unit to caller
}

void printStats(Unit* unit)
{
	// Print name and stats
	cout << unit->getName() << "'s stats:" << endl;
	cout << "Class: " << unit->getUnitClass() << endl;
	cout << "Max HP: " << unit->getMaxHP() << endl;
	cout << "HP: " << unit->getHP() << endl;
	cout << "Power: " << unit->getPower() << endl;
	cout << "Vitality: " << unit->getVitality() << endl;
	cout << "Agility: " << unit->getAgility() << endl;
	cout << "Dexterity: " << unit->getDexterity() << endl;
}

void playRound(Unit* player, Unit* enemy)
{

	// Create pointers for first and second actor
	Unit* first = nullptr;
	Unit* second = nullptr;

	// Check units for higher agility, higher agility unit acts first
	// Player acts first if tied
	// Assign first and second pointer respectively
	if (player->getAgility() >= enemy->getAgility()) 
	{
		first = player;
		second = enemy;
	}
	else
	{
		first = enemy;
		second = player;
	}

	// Print stats of combatants before battle
	printStats(player);
	cout << endl;
	printStats(enemy);
	
	cout << endl;

	system("pause");
	system("cls");

	while (true) // Play turns until one unit dies
	{
		playTurn(first, second); // First unit acts
		if (second->isDead()) // Check if opponent died after attacking
		{
			cout << second->getName() << " has been killed." << endl;
			if (first == player) player->levelUp(second->getUnitClass()); // If winner is player, level up
			system("pause");
			system("cls");
			break; // Break out of loop to start another round
		}
		system("pause");
		system("cls");

		// Second unit acts
		playTurn(second, first);
		if (first->isDead())
		{
			cout << first->getName() << " has been killed." << endl;
			if (second == player) player->levelUp(first->getUnitClass());
			system("pause");
			system("cls");
			break;
		}
		system("pause");
		system("cls");
	}

}

void playTurn(Unit* actor, Unit* target)
{
	// Print hp and actor
	cout << actor->getName() << " has:" << endl;
	cout << "HP: " << actor->getHP() << endl;

	int damage = actor->attack(target); // Holds the damage taken, gets 0 if attack missed

	if (damage >= 1) // Check if attack dealt damage
	{
		cout << actor->getName() << " hits " << target->getName() << " for " << damage << " damage!" << endl;
	}
	else // If attack missed
	{
		cout << actor->getName() << " misses!" << endl;
	}
}
