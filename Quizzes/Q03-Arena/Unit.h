#pragma once
#include <string>
#include <time.h>
#include <iostream>

using namespace std;

class Unit
{

public:
	Unit(string name, string unitClass); // Constructor for player
	Unit(string name, string unitClass, int round); // Constructor for enemy includes round to apply scaling

	// Getters and intuitive functions
	string getName();
	string getUnitClass();
	int getHP();
	int getMaxHP();
	void takeDamage(int value); // Take damage according to parameter
	void healSelf(); // Heals self for 30 percent of max health
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	int attack(Unit* target); // Attack target function
	float checkMultiplierBonus(string targetClass); // Check for bonus damage, returns damage multiplier
	void levelUp(string targetClass); // Increase stats based on passed unit class
	bool isDead(); // Check if health = 0. Returns true if dead

private:
	string mName;
	string mUnitClass;

	// Base stats
	int mHP = 20;
	int mMaxHP = 20;
	int mPower = 10;
	int mVitality = 6;
	int mAgility = 8;
	int mDexterity = 8;
};

