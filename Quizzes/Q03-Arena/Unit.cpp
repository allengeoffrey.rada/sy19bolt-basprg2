#include "Unit.h"

Unit::Unit(string name, string unitClass)
{
	mName = name;
	mUnitClass = unitClass;

	// Give player some bonus stats
	mPower += 3;
	mVitality += 3;
}

Unit::Unit(string name, string unitClass, int round)
{
	mName = name;
	mUnitClass = unitClass;

	for (int i = 0; i < round; i++) // Use level up function to give stat boosts
	{
		levelUp("Enemy"); 
	}

}

string Unit::getName()
{
	return mName;
}

string Unit::getUnitClass()
{
	return mUnitClass;
}

int Unit::getHP()
{
	return mHP;
}

int Unit::getMaxHP()
{
	return mMaxHP;
}

void Unit::takeDamage(int value)
{
	mHP -= value; // Reduce health by value

	if (mHP < 0) mHP = 0; // Minimum health is 0
}

void Unit::healSelf()
{
	int heal = mMaxHP * 0.3; // Heal amount is equal to 30 percent of maximum health
	mHP += heal;
	cout << "You are healed by " << heal << " health!" << endl;
	if (mHP > mMaxHP) mHP = mMaxHP; // Cap health to max health
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getAgility()
{
	return mAgility;
}

int Unit::getDexterity()
{
	return mDexterity;
}

int Unit::attack(Unit* target)
{
	// Compute hitrate. Convert dividend and divisor to float to get a decimal result (percent)
	int hitRate = (float(mDexterity) / float(target->getAgility())) * 100; 

	// Clamp values 
	if (hitRate > 80) hitRate = 80;
	else if (hitRate < 20) hitRate = 20;

	// Random number from 1 - 100
	int randomNumber = rand() % 100 + 1;

	if (randomNumber <= hitRate) // If random number is equal or below to hitrate, attack succeeds
	{
		float damageMultiplier = checkMultiplierBonus(target->getUnitClass()); // Check damage multiplier bonus
		int damage = mPower - target->getVitality() * damageMultiplier; // Compute damage
		if (damage < 1) damage = 1; // Minimum of 1 damage can be dealt
		target->takeDamage(damage); // Cause target to take damage
		return damage; // Return damage value
	}
	else // Else, attack misses
	{
		return 0; // Returns 0 to signify attack missing
	}

}

float Unit::checkMultiplierBonus(string targetClass)
{
	// Check if unit is strong against target, return 1.5 damage multiplier if so
	if (mUnitClass == "Warrior" && targetClass == "Assassin")
	{
		return 1.5;
	}
	else if (mUnitClass == "Assassin" && targetClass == "Mage")
	{
		return 1.5;
	}
	else if (mUnitClass == "Mage" && targetClass == "Warrior")
	{
		return 1.5;
	}
	else // Else return 1 damage multiplier (no bonus)
	{
		return 1;
	}
}

void Unit::levelUp(string targetClass)
{
	// Check for class defeated and grant respective stats
	// Also heals player for 30 percent of their max health
	if (targetClass == "Warrior")
	{
		mPower += 3;
		mVitality += 3;
		cout << "You gain 3 Power and Vitality!" << endl;
		healSelf();
	}
	else if (targetClass == "Assassin")
	{
		mAgility += 3;
		mDexterity += 3;
		cout << "You gain 3 Agility and Dexterity!" << endl;
		healSelf();
	}
	else if (targetClass == "Mage")
	{
		mPower += 5;
		cout << "You gain 5 Power!" << endl;
		healSelf();
	}
	else if (targetClass == "Enemy") // Level up for enemies
	{
		// Give random stats
		mMaxHP += rand() % 2 + 1;
		mHP = mMaxHP;
		mPower += rand() % 1 + 1;
		mVitality += rand() % 1 + 1;
		mAgility += rand() % 1 + 1;
		mDexterity += rand() % 1 + 1;
	}
}

bool Unit::isDead()
{
	if (mHP == 0) return true;
	else return false;
}
