#include "Spell.h"


Spell::Spell(string name)
{
    mName = name;
}

string Spell::getName()
{
    return mName;
}

void Spell::setName(string value)
{
    mName = value;
}

int Spell::activate(Wizard* caster, Wizard* target)
{
    int damage = rand() % (mMaxDamage - mMinDamage + 1) + mMinDamage;
    target->takeDamage(damage);
    caster->useMP(50);
    return damage;

}
