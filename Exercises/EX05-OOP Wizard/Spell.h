#pragma once
#include <string>
#include <time.h>
#include "Wizard.h"

using namespace std;

class Wizard;

class Spell
{

public:
	Spell(string name);

	string getName();
	void setName(string value);
	int activate(Wizard* caster, Wizard* target);

private:
	string mName;
	int mMinDamage = 40;
	int mMaxDamage = 60;
	int mMPCost = 50;

};

