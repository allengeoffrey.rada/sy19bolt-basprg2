#pragma once
#include <string>
#include <time.h>
#include "Spell.h"

using namespace std;

class Spell;

class Wizard
{

public:
	Wizard(string name, Spell* spell);
	~Wizard();

	string getName();
	void setName(string value);
	int getHP();
	void takeDamage(int value);
	int getMP();
	void useMP(int value);
	Spell* getSpell();
	int attack(Wizard* target);
	int generateMP();

private:
	string mName;
	int mHP = 250;
	int mMP = 0;
	int mMinDamage = 10;
	int mMaxDamage = 15;
	Spell* mSpell;

};

