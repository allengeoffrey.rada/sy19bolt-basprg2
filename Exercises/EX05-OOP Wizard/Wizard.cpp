#include "Wizard.h"

Wizard::Wizard(string name, Spell* spell)
{
	mName = name;
	mSpell = spell;
}

Wizard::~Wizard()
{
	delete mSpell;
}

string Wizard::getName()
{
	return mName;
}

void Wizard::setName(string value)
{
	mName = value;
}

int Wizard::getHP()
{
	return mHP;
}

void Wizard::takeDamage(int value)
{
	mHP -= value;

	if (mHP < 0) mHP = 0;
}

int Wizard::getMP()
{
	return mMP;
}

void Wizard::useMP(int value)
{
	mMP -= value;

	if (mMP < 0) mMP = 0;
}

Spell* Wizard::getSpell()
{
	return mSpell;
}

int Wizard::attack(Wizard* target)
{
	int damage = rand() % (mMaxDamage - mMinDamage + 1) + mMinDamage;
	target->takeDamage(damage);
	return damage;
}

int Wizard::generateMP()
{
	int gain = rand() % (20 - 10 + 1) + 10;
	mMP += gain;
	return gain;
}
