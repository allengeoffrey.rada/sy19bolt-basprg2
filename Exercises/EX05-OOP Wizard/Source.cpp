#include <iostream>
#include <time.h>

#include "Wizard.h"
#include "Spell.h"

using namespace std;

void playTurn(Wizard* wizard, Wizard* enemy);

int main()
{
	srand(time(0));
	
	Spell* lightningbolt = new Spell("Lightning Bolt");

	Spell* expelliarmus = new Spell("Expelliarmus");

	Wizard* dumbledore = new Wizard("Dumbledore", expelliarmus);

	Wizard* gandalf = new Wizard("Gandalf", lightningbolt);

	while (true)
	{
		if (dumbledore->getHP() == 0) break;
		playTurn(dumbledore, gandalf);
		system("pause");
		system("cls");
		if (gandalf->getHP() == 0) break;
		playTurn(gandalf, dumbledore);
		system("pause");
		system("cls");
	}

	if (dumbledore->getHP() == 0)
	{
		cout << "Gandalf wins!" << endl;
	}
	else
	{
		cout << "Dumbledore wins!" << endl;
	}

	delete dumbledore;
	delete gandalf;

}

void playTurn(Wizard* wizard, Wizard* enemy)
{
	cout << wizard->getName() << " has:" << endl;
	cout << "HP: " << wizard->getHP() << endl;
	cout << "MP: " << wizard->getMP() << endl;
	if (wizard->getMP() > 50)
	{
		cout << wizard->getName() << " casts " << wizard->getSpell()->getName() << " on " << enemy->getName() << " and deals " << wizard->getSpell()->activate(wizard, enemy) << " damage." << endl;
	}
	else
	{
		cout << wizard->getName() << " attacks " << enemy->getName() << " and deals " << wizard->attack(enemy) << " damage." << endl;
		cout << wizard->getName() << " generates " << wizard->generateMP() << " mana points." << endl;
	}
}