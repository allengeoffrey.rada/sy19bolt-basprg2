#include <iostream>
#include <vector>
#include <time.h>

using namespace std;

vector<string> fillInventory(vector<string>& inventory, const vector<string>& items)
{
	int randomItemNumber;
	for (int i = 0; i < 10; i++)
	{
		randomItemNumber = rand() % items.size();
		inventory.push_back(items[randomItemNumber]);
	}

	return inventory;
}

void printInventory(const vector<string>& inventory)
{
	for (int i = 0; i < inventory.size(); i++)
	{
		cout << inventory[i] << endl;
	}
}

int countItem(const vector<string>& inventory, string item)
{
	int count = 0;
	for (int i = 0; i < 10; i++)
	{
		if (inventory[i] == item)
		{
			count++;
		}
	}

	return count;
}

void removeItem(vector<string>& inventory, int index)
{
	inventory.erase(inventory.begin() + index);
}

int main()
{
	srand(time(NULL));

	vector<string> items{"RedPotion", "Elixir", "EmptyBottle", "BluePotion"};
	vector<string> inventory;

	inventory = fillInventory(inventory, items);
	
	cout << "Contents of inventory:" << endl;
	printInventory(inventory);

	system("pause");

	for (int i = 0; i < items.size(); i++)
	{
		cout << "Number of " << items[i] << "s in inventory: ";
		cout << countItem(inventory, items[i]) << endl;
	}
	
	system("pause");

	cout << endl << "Dropping item in index 1" << endl;
	removeItem(inventory, 1);

	cout << endl << "Contents of inventory:" << endl;
	printInventory(inventory);

}