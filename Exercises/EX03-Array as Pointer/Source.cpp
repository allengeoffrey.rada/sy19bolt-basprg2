#include <iostream>
#include <time.h>

using namespace std;

void fillArray(int* numbers, int size)
{
	for (int i = 0; i < size; i++)
	{
		numbers[i] = rand() % 100;
	}
}

int* createArray(int size)
{
	int* numbers = new int[size];
	fillArray(numbers, size);

	return numbers;
}

int main()
{
	srand(time(0));
	int numbers[5];
	int size = sizeof(numbers) / sizeof(*numbers);

	fillArray(numbers, size);

	for (int i = 0; i < size; i++)
	{
		cout << numbers[i] << endl;
	}

	int* allocatedArray = new int[size];

	allocatedArray = createArray(size);

	for (int i = 0; i < size; i++)
	{
		cout << allocatedArray[i] << endl;
	}

	delete[] allocatedArray;
}