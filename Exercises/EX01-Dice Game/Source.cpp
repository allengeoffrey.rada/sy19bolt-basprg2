#include <iostream>
#include <time.h>

using namespace std;

int betWithReturn(int& gold)
{
	int bet;

	while (true)
	{
		cout << "You have " << gold << " gold, how much would you like to bet?" << endl;
		cin >> bet;
		if (bet > gold || bet <= 0)
		{
			cout << "You have entered an invalid input." << endl;
		}
		else {
			gold -= bet;
			break;
		}
	}

	return bet;
}

void betWithoutReturn(int& gold, int& bet)
{
	while (true)
	{
		cout << "You have " << gold << " gold, how much would you like to bet?" << endl;
		cin >> bet;
		if (bet > gold || bet <= 0)
		{
			cout << "You have entered an invalid input." << endl;
		}
		else {
			gold -= bet;
			break;
		}
	}
}

void rollDice(int& diceOne, int& diceTwo)
{
	diceOne = rand() % 6 + 1;
	diceTwo = rand() % 6 + 1;
}

void payOut(int playerSum, int enemySum, int& playerGold, int playerBet)
{
	int payment;
	if (playerSum == enemySum)
	{
		playerGold += playerBet;
		cout << "It's a draw! You can have your " << playerBet << " gold back!" << endl;
	}
	else if (playerSum == 2)
	{
		payment = playerBet * 3;
		playerGold += payment;
		cout << "You got snake eyes! You receive " << payment << " gold!" << endl;
	}
	else if (enemySum == 2)
	{
		cout << "The enemy got snake eyes! You lose! You get nothing!" << endl;
	}
	else if (playerSum > enemySum)
	{
		payment = playerBet * 2;
		playerGold += payment;
		cout << "You win! You receive " << payment << " gold!" << endl;
	}
	else
	{
		cout << "You lose! You get nothing!" << endl;
	}
}

void playRound(int& playerGold)
{
	int playerBet;

	//playerBet = betWithReturn(playerGold);

	betWithoutReturn(playerGold, playerBet);

	cout << "You're going to bet " << playerBet << " gold." << endl;
	cout << "You have " << playerGold << " gold left." << endl;

	system("pause");

	int playerDiceOne;
	int playerDiceTwo;

	int enemyDiceOne;
	int enemyDiceTwo;

	rollDice(playerDiceOne, playerDiceTwo);
	rollDice(enemyDiceOne, enemyDiceTwo);

	cout << "You rolled the dice and got a " << playerDiceOne << " and a " << playerDiceTwo << "." << endl;
	cout << "The enemy rolled the dice and got a " << enemyDiceOne << " and a " << enemyDiceTwo << "." << endl;

	int playerDiceSum = playerDiceOne + playerDiceTwo;
	int enemyDiceSum = enemyDiceOne + enemyDiceTwo;

	payOut(playerDiceSum, enemyDiceSum, playerGold, playerBet);

	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));

	int playerGold = 1000;

	while (playerGold > 0)
	{
		playRound(playerGold);
	}
	
	cout << "You ran out of money, better luck next time!" << endl;
}