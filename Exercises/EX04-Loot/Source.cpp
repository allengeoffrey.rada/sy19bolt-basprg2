#include <iostream>
#include <time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};

Item* generateRandomItem(Item* lootTable)	
{
	Item* item = new Item;
	int randomNumber = rand() % 5;
	item->name = lootTable[randomNumber].name;
	item->gold = lootTable[randomNumber].gold;

	return item;
}

void enterDungeon(int& gold, Item* lootTable)
{
	gold -= 25;
	int collectedGold = 0;
	int goldMultiplier = 1;
	Item loot; 
	string answer;
	while (true)						
	{
		loot = *generateRandomItem(lootTable);
		cout << "You delve further into the dungeon and found a " << loot.name << "." << endl;
		if (loot.name == "Cursed Stone")
		{
			cout << "The cursed stone takes your life and gold away, unlucky." << endl;
			collectedGold = 0;
			break;
		}
		cout << "It is worth " << loot.gold * goldMultiplier << " gold." << endl;
		collectedGold += loot.gold * goldMultiplier;
		goldMultiplier++;
		cout << "You're carrying " << collectedGold << " gold. The next thing you find will be worth " << goldMultiplier << "x more." << endl;
		cout << "Do you want to go deeper? (y/n)" << endl;
		while (true)
		{
			cin >> answer;
			if (answer == "y")
			{
				break;
			}
			else if (answer == "n")
			{
				break;
			}
			else 
			{
				cout << "Please enter a valid input. (y/n lowercase)" << endl;
			}
		}
		if (answer == "n")
		{
			break;
		}
		system("cls");
	}

	gold += collectedGold;

}

int main()
{
	srand(time(0));

	// Loot table
	Item items[5];
	items[0].name = "Mithril Ore";
	items[0].gold = 100;
	items[1].name = "Short Talon";
	items[1].gold = 50;
	items[2].name = "Thick Leather";
	items[2].gold = 25;
	items[3].name = "Jellopy";
	items[3].gold = 5;
	items[4].name = "Cursed Stone";
	items[4].gold = 0;

	int playerGold = 50;

	while (playerGold >= 25 && playerGold < 500)
	{
		cout << "You currently have " << playerGold << ", but you need 500 gold." << endl;
		cout << "Time to go into the dungeon. You spend 25 gold for supplies." << endl;
		system("pause");
		enterDungeon(playerGold, items);
	}

	cout << "Finally, you're free";
	if (playerGold < 25)
	{
		cout << "..." << endl << "You starve to death and you're free from the endless cycle." << endl;
	}
	else
	{
		cout << "!" << endl << "You go home bringing riches and live a luxurious life." << endl;
	}
}