#pragma once
#include <iostream>
#include "Unit.h"

using namespace std;

class Skill
{

public:
	Skill();

	virtual string getName();
	void setName(string name);
	virtual void activate(Unit* user) = 0;

private:
	string mName;

};

