#pragma once
#include <iostream>
#include <vector>

class Skill;

using namespace std;

class Unit
{

public:
	Unit(string name, vector<Skill*> skills);

	void addHP(int value);
	void addPower(int value);
	void addVitality(int value);
	void addDexterity(int value);
	void addAgility(int value);
	vector<Skill*> getSkills();
	void printStats();

private:
	string mName;
	int mHP = 10;
	int mPower = 10;
	int mVitality = 10;
	int mDexterity = 10;
	int mAgility = 10;
	vector<Skill*> mSkills;

};

