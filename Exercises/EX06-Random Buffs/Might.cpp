#include "Might.h"

Might::Might()
{
	setName("Might");
}

void Might::activate(Unit* user)
{
	user->addPower(2);
}
