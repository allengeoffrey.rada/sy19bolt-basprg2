#include "Concentration.h"

Concentration::Concentration()
{
	setName("Concentration");
}

void Concentration::activate(Unit* user)
{
	user->addDexterity(2);
}
