#include "Unit.h"

Unit::Unit(string name, vector<Skill*> skills)
{
	mName = name;
	mSkills = skills;
}

void Unit::addHP(int value)
{
	mHP += value;
}

void Unit::addPower(int value)
{
	mPower += value;
}

void Unit::addVitality(int value)
{
	mVitality += value;
}

void Unit::addDexterity(int value)
{
	mDexterity += value;
}

void Unit::addAgility(int value)
{
	mAgility += value;
}

vector<Skill*> Unit::getSkills()
{
	return mSkills;
}

void Unit::printStats()
{
	cout << "HP: " << mHP << endl;
	cout << "Power: " << mPower << endl;
	cout << "Vitality: " << mVitality << endl;
	cout << "Dexterity: " << mDexterity << endl;
	cout << "Agility: " << mAgility << endl;
}
