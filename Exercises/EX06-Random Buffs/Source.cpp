#include <iostream>
#include <time.h>
#include "Unit.h"
#include "Skill.h"
#include "Concentration.h"
#include "Haste.h"
#include "Heal.h"
#include "IronSkin.h"
#include "Might.h"

using namespace std;

int main()
{
	srand(time(0));

	vector<Skill*> skills;

	Skill* heal = new Heal();
	Skill* might = new Might();
	Skill* ironSkin = new IronSkin();
	Skill* concentration = new Concentration();
	Skill* haste = new Haste();

	skills.push_back(heal);
	skills.push_back(might);
	skills.push_back(ironSkin);
	skills.push_back(concentration);
	skills.push_back(haste);
	
	Unit* player = new Unit("Allen", skills);

	int randomNumber;


	while (true)
	{
		randomNumber = rand() % 5;
		player->getSkills()[randomNumber]->activate(player);
		cout << player->getSkills()[randomNumber]->getName() << " is used!" << endl;
		player->printStats();
		system("pause");
		system("cls");
	}
}