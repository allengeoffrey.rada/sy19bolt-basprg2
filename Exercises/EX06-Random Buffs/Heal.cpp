#include "Heal.h"

Heal::Heal()
{
	setName("Heal");
}

void Heal::activate(Unit* user)
{
	user->addHP(10);
}
